# Must be in ~/.zshenv
# ZDOTDIR="$XDG_CONFIG_HOME"/zsh 

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# history settings
HISTFILE="$XDG_DATA_HOME"/zsh/history
HISTSIZE=10000
SAVEHIST=10000
setopt HIST_IGNORE_ALL_DUPS

# plugins for fish-like syntax highlighting, suggestions and history search
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

# enable autocompletion
autoload -Uz compinit promptinit
compinit
promptinit

prompt redhat

zstyle ':completion:*' menu select

### ALIASES ###

# bare git repo alias for dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# fast navigation
alias ucpp='cd ~/Documents/university/cpp/'

# nvim
alias vi='nvim'
alias vim='nvim'

# pikaur and pacman
alias syu='pikaur -Syu'    # update standard pkgs and AUR pkgs
alias pacsyu='sudo pacman -Syu' # update only standard pkgs

# colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# colorize ls, ip, diff output
alias ls='ls --color=auto'
alias ip='ip -color=auto'
alias diff='diff --color=auto'

### BINDINGS ###

# set vi mode
#bindkey -v

# map Home, End, Delete for their function
bindkey    '^?'     backward-delete-char
bindkey    '^[[H'   beginning-of-line
bindkey    '^[[F'   end-of-line
bindkey    '^[[3~'  delete-char
bindkey -a '^[[3~'  vi-delete-char
# emacs-style
bindkey '^A' beginning-of-line
bindkey '^E' end-of-line

# bind up and down arrow keys to fish-like search
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# for vi mode
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

### STARSHIP PROMPT ###
eval "$(starship init zsh)"
