<h1 align="center">Sway WM config</h1>

This is a collection of my configuration files.

![Screenshot](./.screenshots/swaywm.png)

Here are some details about my setup:

+ **OS**: [Arch Linux](https://archlinux.org/)
+ **WM**: [SwayWM](https://github.com/swaywm/sway)
+ **Shell**: [zsh](https://wiki.archlinux.org/index.php/Zsh)
+ **Terminal**: [foot](https://codeberg.org/dnkl/foot)
+ **Editor**: [neovim](https://github.com/neovim/neovim/)
+ **Launcher**: [sway-launcher-desktop](https://github.com/Biont/sway-launcher-desktop)
+ **File Manager**: [thunar](https://git.xfce.org/xfce/thunar)
+ **Browser**: [firefox](https://www.mozilla.org/firefox)
